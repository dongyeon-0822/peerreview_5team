## **💻 깃 랩 주소**

[Sangho Kim · GitLab](https://gitlab.com/sy980527)


### ❗ **코드 리뷰 체크 리스트**

- 중복 코드 방지
- 모듈의 재사용성
- 알아보기 쉬운 변수명
