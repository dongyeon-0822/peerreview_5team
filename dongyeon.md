🦊[**GitLab 주소**](https://gitlab.com/dongyeon-0822)


📖 **나의 코드 리뷰 방법**

- 리뷰이로서
    - 나의 코드를 리뷰하는 사람이 알아보기 쉽게 pull request 메시지에 `무슨` 작업을 `왜` 했는지를 작성할 것!

- 리뷰어로서
    - 개선할 점 체크!  
      만약 개선할 점이 있다면 이유를 자세하게 설명하기  
      리팩토링과 클린코드에 대해 집중적으로 리뷰하기 
    - 잘한 점은 많이 칭찬하기
    - 궁금한 점은 질문하기


➡️**리뷰할 나의 개인 프로젝트** 
- [MutsaSNS 프로젝트](https://gitlab.com/dongyeon-0822/likelion-final-project)
